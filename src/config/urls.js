const apiPrefix = "http://localhost:8000/api";
const URL = {
  invoices: {
    get: id => `${apiPrefix}/invoices${id ? `/${id}` : ""}`,
    post: `${apiPrefix}/invoices`,
    delete: id => `${apiPrefix}/invoices${id ? `/${id}` : ""}`,
    put: id => `${apiPrefix}/invoices${id ? `/${id}` : ""}`
  },
  invoiceItems: {
    getAll: id => `${apiPrefix}/invoices/${id}/items`,
    getById: (id, itemId) => `${apiPrefix}/invoices/${id}/items/${itemId}`,
    post: id => `${apiPrefix}/invoices/${id}/items`,
    putById: (id, itemId) => `${apiPrefix}/invoices/${id}/items/${itemId}`,
    deleteById: (id, itemId) => `${apiPrefix}/invoices/${id}/items/${itemId}`
  },
  products: {
    get: id => `${apiPrefix}/products${id ? `/${id}` : ""}`,
    post: `${apiPrefix}/products`,
    delete: id => `${apiPrefix}/products${id ? `/${id}` : ""}`,
    put: id => `${apiPrefix}/products${id ? `/${id}` : ""}`
  },
  customers: {
    get: id => `${apiPrefix}/customers${id ? `/${id}` : ""}`,
    post: `${apiPrefix}/customers`,
    delete: id => `${apiPrefix}/customers${id ? `/${id}` : ""}`,
    put: id => `${apiPrefix}/customers${id ? `/${id}` : ""}`
  }
};

export default URL;

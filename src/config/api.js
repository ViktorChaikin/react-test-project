const headers = {
  "Accept": "application/json",
  "Content-Type" : "application/json"
};

export const add = (url, item) => fetch(url, {
  method: "POST",
  headers,
  body: JSON.stringify(item)
});

export const del = url => fetch(url, {
  method: "DELETE",
  headers
});

export const edit = (url, item) => fetch(url, {
  method: "PUT",
  headers,
  body: JSON.stringify(item)
});

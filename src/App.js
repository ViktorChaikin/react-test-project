import React from "react";
import { Route, Switch } from "react-router";
import { Redirect } from "react-router-dom";

import ToDoListPage from "./pages/ToDoListPage";
import InvoicesPage from "./pages/InvoicesPage";
import ProductsPage from "./pages/ProductsPage";
import CustomersPage from "./pages/CustomersPage";
import Header from "./components/Header";

const App = () => (
  <Header>
    <Switch>
      <Route exact path="/invoices" component={InvoicesPage} />
      <Route path="/products" component={ProductsPage} />
      <Route path="/customers" component={CustomersPage} />
      <Route path="/to-do-list" component={ToDoListPage} />
      <Redirect from="*" to="/invoices" />
    </Switch>
  </Header>
);


export default App;

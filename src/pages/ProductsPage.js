import React from "react";
import ProductList from "../components/InvoicesApp/ProductsList";

const ProductsPage = () => (
  <div className="products-page">
    <ProductList />
  </div>
);

export default ProductsPage;

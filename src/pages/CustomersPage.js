import React from "react";
import CustomersList from "../components/InvoicesApp/CustomersList";

const CustomersPage = () => (
  <div className="customers-page">
    <CustomersList />
  </div>
);

export default CustomersPage;

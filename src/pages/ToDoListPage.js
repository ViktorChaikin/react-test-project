import React, { Component } from "react";
import Button from "../components/ToDoListApp/ButtonComponent";
import ToDoListItem from "../components/ToDoListApp/ToDoListItemComponent";

class ToDoListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: "",
      items: [],
      show: "All"
    };
  }

  onHandleAddItem = () => {
    const { inputText, items } = this.state;
    const newItems = items.slice();

    newItems.push({ id: `todo-item-${Math.random()}`, context: inputText, edit: false, toDo: true });
    this.setState({ inputText: " ", items: newItems });
  };

  onDeleteItem = (id) => {
    const { items } = this.state;
    const newArray = items.filter(element => element.id !== id);

    this.setState({ items: newArray });
  };

  onToDo = (id) => {
    const { items } = this.state;
    const newArray = items.map((element) => {
      if (element.id === id) element.toDo = !element.toDo; return element;
    });

    this.setState({ items: newArray });
  };

  showByCondition = (element) => {
    const { show } = this.state;
    const toDO = show === "ToDo";

    if (show === "All") {
      return this.showItems(element);
    }
    if (element.toDo === toDO) {
      return this.showItems(element);
    }
  };

  showItems = element => (
    <ToDoListItem
      edit={element.edit}
      key={element.id}
      title={element.context}
      items={this.state.items}
      toDo={element.toDo}
      id={element.id}
      deleteItem={this.onDeleteItem}
      onToDo={this.onToDo}
    />
  );

  render() {
    const { inputText, items } = this.state;

    return (
      <div className="to-do-list-page">
        <h1 className="main">ToDoList</h1>
        <input
          type="search"
          id="taskText"
          autoFocus={true}
          value={inputText}
          onChange={
            event => this.setState({ inputText: event.target.value })}
          onKeyUp={(event) => {
            if (event.key === "Enter") {
              this.onHandleAddItem();
            }
          }}

        />
        <Button title="Create new Item" onClick={this.onHandleAddItem} />
        <div className="to-do-list">
          {items.map(this.showByCondition)}
        </div>
        <div className="sort">
          <Button title="Show Done" onClick={() => { this.setState({ show: "Done" }); }} />
          <Button title="Show ToDo" onClick={() => { this.setState({ show: "ToDo" }); }} />
          <Button title="Show All" onClick={() => { this.setState({ show: "All" }); }} />
        </div>
      </div>
    );
  }
}

export default ToDoListPage;

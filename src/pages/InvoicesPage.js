import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import InvoicesList from "../components/InvoicesApp/InvoicesList";
import Button from "../components/ToDoListApp/ButtonComponent";
import AddInvoice from "../components/InvoicesApp/AddInvoice";
import { getInvoices } from "../actions/invoices";
import { getProducts } from "../actions/products";
import { getCustomers } from "../actions/customers";

class InvoicesPage extends Component {
  static propTypes = {
    loadInvoices: PropTypes.func.isRequired,
    loadCustomers: PropTypes.func.isRequired,
    loadProducts: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false
    };
  }

  componentDidMount() {
    const { loadInvoices, loadCustomers, loadProducts } = this.props;

    loadInvoices();
    loadCustomers();
    loadProducts();
  }

  render() {
    const { isVisible } = this.state;
    return (
      <div className="invoice-page">
        <div className={isVisible ? "displayed-modal" : "hidden-modal"}>
          <div className="add-invoice">
            <AddInvoice hide={() => this.setState({ isVisible: false })} />
          </div>
        </div>
        <div className="invoice-page-content">
          <div className="invoice-buttons">
            <Button title="Add Invoice" onClick={() => this.setState({ isVisible: true })} />
          </div>
          <InvoicesList />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadProducts: () => dispatch(getProducts()),
  loadInvoices: () => dispatch(getInvoices()),
  loadCustomers: () => dispatch(getCustomers())
});

export default connect(
  null,
  mapDispatchToProps
)(InvoicesPage);

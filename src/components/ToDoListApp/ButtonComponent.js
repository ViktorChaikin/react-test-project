import React from "react";
import PropTypes from "prop-types";

const ButtonComponent = ({ title, onClick }) => (
  <button className="btn btn-default" type="button" onClick={onClick}>
    {title}
  </button>
);

ButtonComponent.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func
};

ButtonComponent.defaultProps = {
  title: "Default Button",
  onClick: () => {}
};
export default ButtonComponent;

import React, { Component } from "react";
import PropTypes from "prop-types";

import Button from "./ButtonComponent";

class ToDoListItem extends Component {
  static propTypes = {
    edit: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
    deleteItem: PropTypes.func.isRequired,
    onToDo: PropTypes.func.isRequired
  };

  static defaultPropTypes = {
    title: "Default Task",
    items: [],
    edit: false
  };

  constructor(props) {
    super(props);
    this.state = {
      items: props.items,
      title: props.title,
      toDo: "Done",
      edit: props.edit,
      id: props.id
    };
  }

  onEdit = () => {
    const { edit } = this.state;
    this.setState({ edit: !edit });
  };

  onPressToDo = () => {
    const { items, id } = this.state;
    const text = items.find(element => element.id === id).toDo ? "Done" : "ToDo";
    this.setState({ toDo: text });
  };

  writeButtons =() => {
    const { id, onToDo, deleteItem } = this.props;

    return (
      <div className="to-do-list-item-buttons">
        <Button
          title={this.state.toDo}
          className="toDo change"
          id={`ToDo${id}`}
          onClick={() => { onToDo(id); this.onPressToDo(); }}
        />
        <Button title="Edit" type="button" className="edit change" onClick={this.onEdit} />
        <Button title="Delete" type="button" className="delete change" onClick={() => { deleteItem(id); }} />
      </div>
    );
  };

  writeText = () => {
    const { title, items, id } = this.state;

    return (
      <div className={
        items.find(element => element.id === id).toDo ? "to-do-list-item-text" : "to-do-list-item-text done"}
      >
        { title}
      </div>
    );
  };

  saveButton = () => (
    <div>
      <input
        type="search"
        autoFocus={true}
        value={this.state.title}
        onChange={
          event => this.setState({ title: event.target.value })}
        onKeyUp={(event) => {
          if (event.key === "Enter") {
            this.onEdit();
          }
        }}

      />
      <Button type="button" title="Save" onClick={this.onEdit} />
    </div>
  );

  render() {
    const { edit } = this.state;

    return (
      <div className="to-do-list-item">
        { (edit ? this.saveButton() : this.writeText())}
        {this.writeButtons()}
      </div>
    );
  }
}


export default ToDoListItem;

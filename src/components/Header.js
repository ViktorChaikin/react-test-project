import React, { Component } from "react";
import Link from "react-router-dom/es/Link";
import PropTypes from "prop-types";

class Header extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      active: "invoices"
    };
  }

  onChangePage = (page) => {
    this.setState({ active: page });
  };

  render() {
    const { children } = this.props;
    const { active } = this.state;

    return (
      <header>
        <div className="header">
          <Link
            className={`header-item ${active === "customers" && "active"}`}
            to="/customers"
            onClick={() => this.onChangePage("customers")}
          >
            {"Customers"}
          </Link>
          <Link
            className={`header-item ${active === "invoices" && "active"}`}
            to="/invoices"
            onClick={() => this.onChangePage("invoices")}
          >
            {"Invoices"}
          </Link>
          <Link
            className={`header-item ${active === "products" && "active"}`}
            to="/products"
            onClick={() => this.onChangePage("products")}
          >
            {"Products"}
          </Link>
        </div>
        {children}
      </header>
    );
  }
}

export default Header;

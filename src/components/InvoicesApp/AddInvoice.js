import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Select from "react-select";
import Button from "../ToDoListApp/ButtonComponent";
import { createInvoice } from "../../actions/invoices";

class AddInvoice extends Component {
  static propTypes = {
    hide: PropTypes.func.isRequired,
    addInvoice: PropTypes.func.isRequired,
    products: PropTypes.arrayOf(Array).isRequired,
    customers: PropTypes.arrayOf(Array).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      invoiceItems: [],
      discount: "",
      product: null,
      customer: null,
      quantity: "",
      totalPrice: 0,
      itemId: 1
    };
  }

  componentDidMount() {
    this.setState({ isLoaded: true });
  }

  setTotalPrice = () => {
    const { invoiceItems } = this.state;
    const price = invoiceItems.reduce((sum, item) => sum + item.price, 0);
    this.setState({ totalPrice: price });
  };

  onEditInvoiceItem = (id) => {
    const { invoiceItems } = this.state;
    console.log(`Edit item ${id}`);
    const items = invoiceItems.map((item) => {
      if (item.id === id) {
        item.edit = !item.edit;
      }
      return item;
    });

    return new Promise((resolve) => {
      this.setState({ invoiceItems: items });
      resolve();
    }).then(() => this.setTotalPrice());
  };

  onDeleteInvoiceItem =(invoiceItem) => {
    const { invoiceItems } = this.state;
    const newItems = invoiceItems.filter(item => item.id !== invoiceItem.id);

    this.setState({ invoiceItems: newItems }, this.setTotalPrice);
  };

  getInvoiceItem = (item, index) => {
    const { quantity } = this.state;

    return (
      <tr key={index} className={index % 2 === 0 ? "green" : "yellow"}>
        <td>{index}</td>
        <td>{item.product.label}</td>
        <td>
          {item.edit
            ? (
              <input
                type="number"
                min="1"
                value={quantity}
                onChange={event => this.setState({ quantity: event.target.value })}
              />
            ) : item.quantity}
        </td>
        <td><i className="fas fa-dollar-sign">{`  ${item.price}`}</i></td>
        <td>
          <button type="button" className="button-without-styles" onClick={() => this.onEditInvoiceItem(item.id)}>
            <i className="fas fa-edit scale cursor" />
          </button>
          <button type="button" className="button-without-styles" onClick={() => this.onDeleteInvoiceItem(item)}>
            <i className="fas fa-trash-alt scale cursor" />
          </button>
        </td>
      </tr>
    );
  };

  showInvoiceItemsList = () => {
    const { invoiceItems } = this.state;

    return (
      <table>
        <tbody>
          <tr>
            <th>№</th>
            <th>Product</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Change</th>
          </tr>
          { invoiceItems.map((invoice, index) => this.getInvoiceItem(invoice, index + 1)) }
        </tbody>
      </table>
    );
  };

  onAddInvoice = () => {
    const { customer, discount, totalPrice, invoiceItems } = this.state;
    const { addInvoice, hide } = this.props;

    if (customer !== null && discount !== "" && totalPrice !== "" && invoiceItems[0] !== undefined) {
      const invoice = {
        customer_id: customer.id,
        discount,
        total: Number((totalPrice * (1 - discount / 100)).toFixed(2))
      };
      const transformedInvoiceItems = invoiceItems.map(item => ({
        product_id: item.product.id,
        quantity: item.quantity
      }));

      addInvoice(invoice, transformedInvoiceItems);
      hide();
      this.setState({ invoiceItems: [] });
    } else {
      console.log("NOt all fields are filled");
    }
  };

  onAddInvoiceItem = () => {
    const { customer, product, quantity, discount, invoiceItems, itemId } = this.state;
    if (customer !== null && product !== null && quantity !== "" && discount !== 0) {
      const price = Number((product.price * quantity).toFixed(2));
      const items = invoiceItems.slice();
      items.push({ product, quantity, price, id: itemId, edit: false });
      console.log(items);

      this.setState({
        product: null,
        quantity: "",
        invoiceItems: items,
        itemId: itemId + 1
      }, this.setTotalPrice);
    } else {
      console.log(" You have got problems with input");
    }
  };

  render() {
    const { customers, products, hide } = this.props;
    const { isLoaded, totalPrice, discount, quantity, product } = this.state;

    return isLoaded ? (
      <div className="add-invoice-container">
        <h1>Invoice</h1>

        <div className="invoice-container-buttons">
          <Select
            className="select-field"
            placeholder="Customers"
            options={customers.map(client => ({ id: client.id, label: client.name }))}
            onChange={selectedOption => this.setState({ customer: selectedOption })}
          />
          <input
            className="select-field"
            type="number"
            value={discount}
            placeholder="Discount"
            max="99"
            min="0"
            onChange={event => this.setState({ discount: event.target.value })}
          />
        </div>

        <h2>Invoice Items</h2>
        <div className="invoice-items-list">
          <div className="invoice-container-buttons">
            <Select
              className="select-field"
              placeholder="Products"
              options={products.map(prod => ({ id: prod.id, label: prod.name, price: prod.price }))}
              onChange={selectedOption => this.setState({ product: selectedOption })}
            />
            <input
              className="select-field"
              type="number"
              min="1"
              value={quantity}
              placeholder="Quantity"
              onChange={event => this.setState({ quantity: event.target.value })}
            />
            <div className="product-price">{`Price : ${product === null ? 0 : product.price}`}</div>
            <Button title="Add" onClick={this.onAddInvoiceItem} />
          </div>
          <div className="invoice-items-list-container">
            { this.showInvoiceItemsList()}
          </div>
          <div className="invoice-container-buttons">
            <div className="total-price"> Total Price :
              <i className="fas fa-dollar-sign">
                {`  ${(discount === "") ? totalPrice : (totalPrice * Number(1 - discount / 100)).toFixed(2)}`}
              </i>
            </div>
            <Button title="Add Invoice" onClick={this.onAddInvoice} />
            <Button title="Close" onClick={hide} />
          </div>
        </div>

      </div>
    ) : <div>Is Not Loaded Yet</div>;
  }
}

const mapStateToProps = store => ({
  products: store.products.products,
  customers: store.customers.customers,
  invoices: store.invoices.invoices
});

const mapDispatchToProps = dispatch => ({
  addInvoice: (invoice, invoiceItems) => dispatch(createInvoice(invoice, invoiceItems))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddInvoice);

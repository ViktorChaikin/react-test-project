import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteInvoiceById } from "../../actions/invoices";

class InvoiceItem extends Component {
	static propTypes = {
		invoice: PropTypes.object.isRequired,
		index: PropTypes.number.isRequired,
		deleteInvoice: PropTypes.func.isRequired,
		customers: PropTypes.arrayOf(Array).isRequired
	};

	constructor(props) {
		super(props);
		this.state = {
			customerId: props.invoice.customer_id,
			isLoading: true,
			discount: props.invoice.discount,
			total: props.invoice.total
		};
	}

	componentDidMount() {
		this.setState({ isLoading: false });
	}

	getCustomerName = (id) => {
		const { customers } = this.props;
		const { isLoading } = this.state;
		let customer = null;

		if (!isLoading) {
			customer = customers.find(cust => cust.id === id);
		}

		return customer && customer.name;
	};

	render() {
		const { discount, total, customerId, isLoading } = this.state;
		const { invoice, deleteInvoice, index } = this.props;
		const customerName = isLoading ? "hello" : this.getCustomerName(customerId);

		return isLoading ? <div>IsLoading...</div>
			: (
				<div className={`invoice-list-row ${(index + 1) % 2 === 0 ? "purple" : "orange"}`}>
					<div className="invoice-list-row-item">{index + 1}</div>
					<div className="invoice-list-row-item">{customerName}</div>
					<div className="invoice-list-row-item">
						{`${discount} `}
						<i className="fas fa-percent small" />
					</div>
					<div className="invoice-list-row-item">
						<i className="fas fa-dollar-sign " />
						{` ${total}`}
					</div>
					<div className="invoice-list-row-item">
						<button
							type="button"
							className={`button-without-styles ${(index + 1) % 2 === 0 ? "purple" : "orange"}`}
							onClick={() => deleteInvoice(invoice.id)}
						>
							<i className="fas fa-trash-alt scale cursor" />
						</button>
						<button
							type="button"
							className={`button-without-styles ${(index + 1) % 2 === 0 ? "purple" : "orange"}`}
						>
							<i className="fas fa-caret-down cursor fa-2x" />
						</button>
					</div>
				</div>
			);
	}
}

const mapStateToProps = store => ({
	customers: store.customers.customers
});

const mapDispatchToProps = dispatch => ({
	deleteInvoice: id => dispatch(deleteInvoiceById(id))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(InvoiceItem);

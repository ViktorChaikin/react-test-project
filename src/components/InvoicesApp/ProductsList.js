import React, { Component } from "react";
import { connect } from "react-redux";
import { getProducts, createProduct } from "../../actions/products";
import Button from "../ToDoListApp/ButtonComponent";
import ProductItem from "./ProductItem";

class ProductsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      price: undefined
    };
    props.getProducts();
  }

  onHandleAddItem = (price, name) => {
    const { createProduct } = this.props;
    if (price !== "" && name !== "") {
      createProduct({ name, price: Number(price).toFixed(2), edit: false });
      this.setState({ name: "", price: "" });
    }
  };

  render() {
    const { products } = this.props;
    const { name, price } = this.state;

    return (
      <div className="product-list">
        <div className="input-field">
          <div>
            <input
              onKeyUp={event => event.key === "Enter" && this.onHandleAddItem(price, name)}
              value={name}
              placeholder="Name"
              type="text"
              onChange={event => this.setState({ name: event.target.value })}
            />
          </div>
          <div>
            <input
              value={price}
              placeholder="Price"
              onKeyUp={event => event.key === "Enter" && this.onHandleAddItem(price, name)}
              type="number"
              onChange={event => this.setState({ price: event.target.value })}
            />
          </div>
          <Button
            title="Add"
            onClick={() => { this.onHandleAddItem(price, name); }}
          />
        </div>
        <div className="product-list-header">
          <div className="product-list-header-item">№</div>
          <div className="product-list-header-item">Product Name</div>
          <div className="product-list-header-item">Price</div>
          <div className="product-list-header-item">Change</div>
        </div>
        {
          products.map((product, index) => <ProductItem key={product.id} index={index} product={product} />)
        }
      </div>
    );
  }
}

const mapStateToProps = store => ({
  products: store.products.products
});

const mapDispatchToProps = dispatch => ({
  getProducts: () => dispatch(getProducts()),
  createProduct: data => dispatch(createProduct(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsList);

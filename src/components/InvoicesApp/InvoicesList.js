import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import InvoiceItem from "./InvoiceItem";

class InvoicesList extends Component {
  static propTypes = {
    invoices: PropTypes.arrayOf(Array).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { invoices } = this.props;

    return (
      <div className="invoice-list">
        <div className="invoice-list-container">
          <div className="invoice-list-row">
            <div className="invoice-list-row-item">№</div>
            <div className="invoice-list-row-item">Customer</div>
            <div className="invoice-list-row-item">Discount</div>
            <div className="invoice-list-row-item">Total</div>
            <div className="invoice-list-row-item">Change</div>
          </div>
          { invoices.map((item, index) => <InvoiceItem key={item.id} invoice={item} index={index} />)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({//  Data from Store
  invoices: state.invoices.invoices
});


export default connect(
  mapStateToProps
)(InvoicesList);

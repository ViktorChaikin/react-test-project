import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteProductById, editProductById } from "../../actions/products";

class ProductItem extends Component {
	static propTypes = {
		product: PropTypes.objectOf(Object),
		index: PropTypes.number.isRequired,
		editProduct: PropTypes.func.isRequired,
		deleteProduct: PropTypes.func.isRequired
	};

	static defaultProps = {
		product: {}
	};

	constructor(props) {
		super(props);
		this.state = {
			editStatus: false,
			inputName: props.product.name || "",
			inputPrice: props.product.price || undefined
		};
	}

	render() {
		const { index, product, editProduct, deleteProduct } = this.props;
		const { editStatus, inputName, inputPrice } = this.state;

		return (
			<div key={product.id} className={`product-item ${index % 2 === 0 ? "yellow" : "green"}`}>
				{editStatus ? (
					<div className="product-item-data-edit">
						<input
							value={inputName}
							className="edit-input"
							placeholder="Name "
							type="text"
							onKeyUp={event => event.key === "Enter" && editProduct(product.id, { name: inputName, price: Number(inputPrice) })}
							onChange={event => this.setState({ inputName: event.target.value })}
						/>
						<input
							className="edit-input"
							value={inputPrice}
							placeholder="Price"
							type="number"
							onKeyUp={event => event.key === "Enter" && editProduct(product.id, { name: inputName, price: Number(inputPrice) })}
							onChange={event => this.setState({ inputPrice: event.target.value })}
						/>
					</div>
				) : (
					<div className="product-item-data">
						<div>{index + 1}</div>
						<div>{`${product.name}`}</div>
						<div >
							<i className="fas fa-dollar-sign" />
							{product.price}
						</div>
					</div>
				)}
				<div className="product-item-buttons">
					<button
						type="button"
						className="button-without-styles"
						onClick={() => {
							this.setState(prevState => ({ editStatus: !prevState.editStatus }));
							editProduct(product.id, { name: inputName, price: Number(inputPrice) });
							console.log("edit");
							console.log(editStatus);
						}}
					>
						<i className="fas fa-edit scale" />
					</button>

					<button className="button-without-styles" type="button" onClick={() => deleteProduct(product.id)}>
						<i className="fas fa-trash-alt scale" />
					</button>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	deleteProduct: id => dispatch(deleteProductById(id)),
	editProduct: (id, data) => dispatch(editProductById(id, data))
});

export default connect(
	null,
	mapDispatchToProps
)(ProductItem);

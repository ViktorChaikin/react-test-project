import React, { Component } from "react";
import { connect } from "react-redux";
import { getCustomers, createCustomer } from "../../actions/customers";
import CustomerItem from "./CustomerItem";
import Button from "../ToDoListApp/ButtonComponent";

class CustomersList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			address: "",
			phone: ""
		};
		props.getCustomers();
	}

	onAddCustomer = (name, address, phone) => {
		const { createCustomer } = this.props;
		if (name !== "" && address !== "" && phone !== "") {
			createCustomer({ name, address, phone });
			this.setState({ name: "", address: "", phone: "" });
		}
	};

	render() {
		const { customers } = this.props;
		const { name, address, phone } = this.state;
		return (
			<div className="customers-list">
				<div className="input-field">
					<input
						onKeyUp={event => event.key === "Enter" && this.onAddCustomer(name, address, phone)}
						value={name}
						className="input-field-item"
						placeholder="Name"
						type="text"
						onChange={event => this.setState({ name: event.target.value })}
					/>

					<input
						onKeyUp={event => event.key === "Enter" && this.onAddCustomer(name, address, phone)}
						value={address}
						placeholder="Address"
						className="input-field-item"
						type="text"
						onChange={event => this.setState({ address: event.target.value })}
					/>

					<input
						value={phone}
						className="input-field-item"
						placeholder="Phone"
						onKeyUp={event => event.key === "Enter" && this.onAddCustomer(name, address, phone)}
						type="number"
						onChange={event => this.setState({ phone: event.target.value })}
					/>
					<Button
						title="Add"
						onClick={() => {
							this.onAddCustomer(name, address, phone);
						}}
					/>
				</div>
				{
					customers.map((customer, index) => <CustomerItem key={customer.id} index={index} customer={customer} />)
				}
			</div>
		);
	}
}

const mapStateToProps = store => ({
	customers: store.customers.customers
});

const mapDispatchToProps = dispatch => ({
	getCustomers: () => dispatch(getCustomers()),
	createCustomer: data => dispatch(createCustomer(data))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CustomersList);

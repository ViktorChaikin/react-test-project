import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { deleteCustomerById, editCustomerById } from "../../actions/customers";

class CustomerItem extends Component {
	static propTypes = {
		customer: PropTypes.objectOf(Object),
		editCustomer: PropTypes.func.isRequired,
		deleteCustomer: PropTypes.func.isRequired,
		index: PropTypes.number.isRequired
	};

	static defaultProps = {
		customer: {}
	};

	constructor(props) {
		super(props);
		this.state = {
			edit: false,
			inputName: props.customer.name || "",
			inputAddress: props.customer.address || "",
			inputPhone: props.customer.phone || undefined
		};
	}

	render() {
		const { customer, editCustomer, deleteCustomer, index } = this.props;
		const { inputName, inputAddress, inputPhone, edit } = this.state;
		const editState = edit;

		return (
			<div key={customer.id} className={`customers-item ${index % 2 === 0 ? "yellow" : "blue"}`}>
				<div className="customers-item-data">
					{editState ? (
						<input
							value={inputName}
							placeholder="Name "
							type="text"
							onChange={event => this.setState({ inputName: event.target.value })}
						/>
					) : <div>{customer.name}</div>}
					{editState ? (
						<input
							value={inputAddress}
							placeholder="Address "
							type="text"
							onChange={event => this.setState({ inputAddress: event.target.value })}
						/>
					) : <div>{customer.address}</div>}
					{editState ? (
						<input
							value={inputPhone}
							placeholder="Phone"
							type="number"
							onChange={event => this.setState({ inputPhone: event.target.value })}
						/>
					) : <div>{customer.phone}</div>}
				</div>
				<div className="customers-item-buttons">
					<button
						className="button-without-styles"
						type="button"
						onClick={() => {
							this.setState({ edit: !editState });
							editCustomer(customer.id, { name: inputName, address: inputAddress, phone: Number(inputPhone) });
						}}
					>
						<i className="fas fa-edit scale" />
					</button>
					<button type="button" className="button-without-styles" onClick={() => deleteCustomer(customer.id)}>
						<i className="fas fa-trash-alt scale" />
					</button>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	deleteCustomer: id => dispatch(deleteCustomerById(id)),
	editCustomer: (id, data) => dispatch(editCustomerById(id, data))
});

export default connect(
	null,
	mapDispatchToProps
)(CustomerItem);

import { IMPORT_ALL_INVOICES, CREATE_INVOICE, EDIT_INVOICE, DELETE_INVOICE_BY_ID } from "./actionTypes";
import URL from "../config/urls";
import { del, edit, add } from "../config/api";

export const getInvoices = () => (dispatch) => {
  //  console.log("Getting invoices");
  fetch(URL.invoices.get())
    .then(res => res.json())
    .then(data => dispatch({
      type: IMPORT_ALL_INVOICES,
      invoices: data
    }));
};

export const createInvoice = (invoice, invoiceItems) => dispatch => add(URL.invoices.post, invoice)
  .then(res => res.json())
  .then(invoiceData => new Promise((resolve) => {
    const items = [];

    invoiceItems.map(item => add(URL.invoiceItems.post(invoiceData.id), item)
      .then((itemData => itemData.json()))
      .then((data) => { items.push(data); }));

    resolve({ invoice: invoiceData, items });
  }))
  .then(data => dispatch({
    type: CREATE_INVOICE,
    invoice: data.invoice,
    items: data.items
  }));

export function editInvoice(id, data) {
  return dispatch => edit(URL.invoices.put(id), data)
    .then(res => res.json())
    .then(editedData => dispatch({
      type: EDIT_INVOICE,
      invoice: editedData
    }));
}

export function deleteInvoiceById(id) {
  return dispatch => del(URL.invoices.delete(id))
    .then(res => res.json())
    .then(() => dispatch({
      type: DELETE_INVOICE_BY_ID,
      id
    }));
}

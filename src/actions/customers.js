import {
  IMPORT_ALL_CUSTOMERS,
  DELETE_CUSTOMER_BY_ID,
  CREATE_CUSTOMER,
  EDIT_CUSTOMER
} from "./actionTypes";
import URL from "../config/urls";
import { add, del, edit } from "../config/api";

export const getCustomers = () => (dispatch) => {
  return fetch(URL.customers.get())
    .then(res => res.json())
    .then(data => dispatch({
      type: IMPORT_ALL_CUSTOMERS,
      customers: data
    }));
};

export const deleteCustomerById = id => dispatch => del(URL.customers.delete(id))
  .then(res => res.json())
  .then(data => dispatch({
    type: DELETE_CUSTOMER_BY_ID,
    customer: data
  }));

export const createCustomer = data => dispatch => add(URL.customers.post, data)
  .then(res => res.json())
  .then(cus => dispatch({
    type: CREATE_CUSTOMER,
    customer: cus
  }));

export const editCustomerById = (id, data) => dispatch => edit(URL.customers.put(id), data)
  .then(res => res.json())
  .then(changedCustomer => dispatch({
    type: EDIT_CUSTOMER,
    customer: changedCustomer
  }));

import { IMPORT_ALL_PRODUCTS, DELETE_PRODUCT_BY_ID, CREATE_PRODUCT, EDIT_PRODUCT } from "./actionTypes";
import URL from "../config/urls";
import { add, del, edit } from "../config/api";

export const getProducts = () => (dispatch) => {
  fetch(URL.products.get())
    .then(res => res.json())
    .then(data => dispatch({
      type: IMPORT_ALL_PRODUCTS,
      products: data
    }));
};

export function deleteProductById(id) {
  return dispatch => del(URL.products.delete(id))
    .then(res => res.json())
    .then(data => dispatch({
      type: DELETE_PRODUCT_BY_ID,
      product: data
    }));
}

export function editProductById(id, data) {
  return dispatch => edit(URL.products.put(id), data)
    .then(res => res.json())
    .then(changedData => dispatch({
      type: EDIT_PRODUCT,
      product: changedData
    }));
}

export function createProduct(data) {
  return dispatch => add(URL.products.post, data)
    .then(res => res.json())
    .then(product => dispatch({
      type: CREATE_PRODUCT,
      product
    }));
}

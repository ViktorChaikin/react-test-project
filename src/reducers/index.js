import invoices from "./invoices";
import products from "./products";
import customers from "./customers";

export default {
  invoices,
  products,
  customers
};

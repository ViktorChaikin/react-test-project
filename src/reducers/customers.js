import {
  IMPORT_ALL_CUSTOMERS,
  DELETE_CUSTOMER_BY_ID,
  EDIT_CUSTOMER,
  CREATE_CUSTOMER
} from "../actions/actionTypes";

const initialState = {
  customers: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case IMPORT_ALL_CUSTOMERS:
      return {
        ...state,
        customers: action.customers
      };
    case DELETE_CUSTOMER_BY_ID:
      return {
        ...state,
        customers: state.customers.filter(customer => customer.id !== action.customer.id)
      };
    case EDIT_CUSTOMER:
      return {
        ...state,
        customers: state.customers.map(customer => ((customer.id !== action.customer.id) ? customer : action.customer))
      };
    case CREATE_CUSTOMER:
      return {
        ...state,
        customers: [...state.customers, action.customer]
      };
    default: return state;
  }
}

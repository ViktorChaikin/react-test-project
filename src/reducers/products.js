import { IMPORT_ALL_PRODUCTS, DELETE_PRODUCT_BY_ID, CREATE_PRODUCT, EDIT_PRODUCT } from "../actions/actionTypes";

const initialState = {
  products: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case IMPORT_ALL_PRODUCTS:
      return {
        ...state,
        products: action.products
      };
    case DELETE_PRODUCT_BY_ID:
      return {
        ...state,
        products: state.products.filter(product => product.id !== action.product.id)
      };
    case CREATE_PRODUCT:
      return {
        ...state,
        products: [...state.products, action.product]
      };
    case EDIT_PRODUCT:
      return {
        ...state,
        products: state.products.map(product => (product.id === action.product.id ? action.product : product))
      };
    default: return state;
  }
}

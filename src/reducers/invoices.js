import { IMPORT_ALL_INVOICES, CREATE_INVOICE, EDIT_INVOICE, DELETE_INVOICE_BY_ID } from "../actions/actionTypes";

const initialState = {
  isLoading: false,
  invoices: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    // case INVOICES_REQUEST:
    //   return {
    //     ...state,
    //     isLoading: true
    //   };
    case IMPORT_ALL_INVOICES:
      return {
        ...state,
        invoices: action.invoices,
        isLoading: false
      };
    case CREATE_INVOICE:
      console.log("Invoice");
      action.invoice.items = action.items;
      console.log(action.invoice);
      return {
        ...state,
        invoices: [...state.invoices, action.invoice]
      };
    case EDIT_INVOICE:
      return {
        ...state,
        invoices: state.invoices.map(invoice => (invoice.id === action.invoice.id ? action.invoice : invoice))
      };
    case DELETE_INVOICE_BY_ID:
      return {
        ...state,
        invoices: state.invoices.filter(invoice => invoice.id !== action.id)
      };
    default: return state;
  }
}

module.exports = {
  extends: "airbnb",
  env: {
    browser: true,
    es6: true,
  },
  globals: {
    XcooBee: true,
  },
  parser: "babel-eslint",
  rules: {
    "linebreak-style": ["off", "unix"],
    "max-len": ["error", { code: 130, ignoreUrls: true }],
    "no-param-reassign": "off",
    "no-underscore-dangle": "off",
    "object-curly-newline": "off",
    quotes: ["error", "double", { avoidEscape: true }],
    "jsx-a11y/label-has-associated-control": "off",
    "jsx-a11y/label-has-for": "off",
    "react/jsx-filename-extension": "off",
    "comma-dangle": ["error", "never"],
    "no-console": "off",
    "react/forbid-prop-types": "off",
    "indent": "off",
    "no-tabs": "off",
    "no-mixed-spaces-and-tabs": "off",
    "spaced-comment": "off",
    "react/jsx-indent": "off",
    "react/jsx-indent-props": "off"
  },
};
